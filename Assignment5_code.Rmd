---
title: "STA 242 Assignment 5"
author: "Mutian Niu (999529375)"
date: "May 20, 2015"
output: pdf_document
---


```{r}
setwd("~/Documents/STA_242/Assignment5")
library("data.table")
library("ff")
library("parallel")
library("doParallel")
cl <- makeCluster(3, "FORK")
registerDoParallel(cl)
getDoParWorkers()

farenames = list.files(pattern = "trip_fare.*csv$")
tripnames = list.files(pattern = "trip_data.*csv$")

read_one_file = function(x)
{
  fread(x, header = TRUE, select = c(" surcharge", " tolls_amount", " total_amount"))
}

fare = clusterApplyLB(cl, farenames, read_one_file)

fare = rbindlist(fare)

trip = rbindlist(clusterApplyLB(cl, tripnames, read_one_file))

trip = rbindlist(trip)

stopCluster(cl)
```