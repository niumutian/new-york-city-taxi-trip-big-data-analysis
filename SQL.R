time0 = proc.time()

library("RMySQL")
con = dbConnect(MySQL(), user = 'root', password = '1234', dbname = 'nyctaxi', host = "localhost")
dbListResults(con)

dbGetQuery(con, "CREATE TABLE trip1 (id INT, INDEX (id), pickup_datetime DATETIME, 
           dropoff_datetime DATETIME, trip_distance DOUBLE, pickup_long DOUBLE, 
           pickup_lat DOUBLE, dropoff_long DOUBLE, dropoff_lat DOUBLE, time_sec DOUBLE, 
           earth_distance DOUBLE)ENGINE = MYISAM")


dbGetQuery(con, "CREATE TABLE fare1 (id INT, INDEX (id), surcharge DOUBLE, tolls_amount DOUBLE, 
           total_amount DOUBLE, total_less_toll DOUBLE)ENGINE = MYISAM")

# read data into database
dbGetQuery(con, "LOAD DATA LOCAL INFILE '/Users/mutianniu/Documents/STA_242/Assignment5/tripdata/trip_final.csv' INTO TABLE trip1 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (id, pickup_datetime, dropoff_datetime, trip_distance, pickup_long, pickup_lat, dropoff_long, dropoff_lat) SET time_sec = TIMESTAMPDIFF(SECOND, pickup_datetime, dropoff_datetime), earth_distance = (3958.748 * ACOS(SIN(RADIANS(dropoff_lat)) * SIN(RADIANS(pickup_lat)) + COS(RADIANS(dropoff_lat)) * COS(RADIANS(pickup_lat)) * COS(RADIANS(pickup_long) - RADIANS(dropoff_long))))")

dbGetQuery(con, "LOAD DATA LOCAL INFILE '/Users/mutianniu/Documents/STA_242/Assignment5/tripfare/fare_final.csv' INTO TABLE fare1 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (id, surcharge, tolls_amount, total_amount) SET total_less_toll = total_amount - tolls_amount")

dbGetQuery(con, "CREATE TABLE alldata AS SELECT trip_distance, pickup_long, pickup_lat, dropoff_long, dropoff_lat, time_sec, earth_distance, surcharge, total_less_toll FROM trip1 INNER JOIN fare1 ON trip1.id = fare1.id")

time1 = proc.time()
read_data_time = time1 - time0
read_data_time


## delete outliers
dbGetQuery(con, "DELETE FROM alldata WHERE total_less_toll > 500 OR total_less_toll < 0 
           OR pickup_long = 0 OR pickup_lat = 0 OR dropoff_long = 0 OR dropoff_lat = 0 
           OR trip_distance > 100 OR trip_distance < earth_distance")

time2 = proc.time()
delete_data_time = time2 - time1
delete_data_time

# save ordered total_less_toll into a different table
dbGetQuery(con, "CREATE TABLE amount_diff_order AS SELECT total_less_toll FROM alldata ORDER BY total_less_toll")

## compute the deciles
n = dbGetQuery(con,"SELECT count(*) FROM amount_diff_order")

position = round(n[[1]]*seq(0.1, 1, length.out = 10))

deciles = sapply(position, function(i) 
  dbGetQuery(con, paste0("SELECT * FROM amount_diff_order LIMIT ",i-1,",1")))

deciles


## simple linear regression

dbGetQuery(con, "SELECT @n := COUNT(*), @meanX :=AVG(time_sec), @sumX :=SUM(time_sec), 
           @sumXX := SUM(time_sec * time_sec), @meanY :=AVG(total_less_toll), @sumY :=SUM(total_less_toll), 
           @sumYY := SUM(total_less_toll * total_less_toll), @sumXY := SUM(time_sec * total_less_toll) 
           FROM alldata")

# Slope, Intercept, R square
slope = dbGetQuery(con, "SELECT @b := (@n * @sumXY - @sumX * @sumY) / (@n * @sumXX - @sumX * @sumX)")
slope

intercept = dbGetQuery(con, "SELECT @a := (@meanY - @b*@meanX)") 
intercept

r_square = dbGetQuery(con, "SELECT POWER(((@n*@sumXY - @sumX*@sumY)/SQRT((@n*@sumXX - @sumX*@sumX) * (@n*@sumYY - @sumY*@sumY))), 2)")
r_square

time3 = proc.time()
computation_time = time3 - time2
computation_time

total_time = read_data_time + delete_data_time + computation_time
total_time